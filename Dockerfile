ARG SOURCE_DOCKER_REGISTRY=localhost:5000

#FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_openjdk:8u AS opt_openjdk

#FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_tools:19.04 AS build

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_base:19.04
#FROM openjdk:8-jre-alpine

#COPY --from=opt_openjdk /opt/ /opt/

#ENV PATH /opt/java/bin/:/opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
#ENV LD_LIBRARY_PATH /opt/lib/
#ENV INSTALL4J_JAVA_HOME /opt/java/

#RUN which java && false

RUN apt install -y openjdk-8-jre wget intel-opencl-icd xdg-utils desktop-file-utils locales firefox

RUN update-desktop-database

RUN locale-gen en_US.UTF-8

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch && wget -t 1 http://localhost:8000/Cytoscape_3_7_2_unix.sh || \
                       wget -t 3 https://github.com/cytoscape/cytoscape/releases/download/3.7.2/Cytoscape_3_7_2_unix.sh && \
    cd /tmp/scratch && bash ./Cytoscape_3_7_2_unix.sh -q && \
    cd / && rm -rf /tmp/scratch